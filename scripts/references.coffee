module.exports = (robot) ->

  spoiler = [
  	"Spoiler!? Corram para as montanhas!",
  	"Spoiler!? Nããããããããoooooooooo!!!!!!"
  ]

  civic = [
  	"Civic? Honda Civic!? Melhor carro do mundo!",
  	"Quer um carro pra durar 10 anos? Compre um Honda Civic!",
  	"A primeira revisão do Honda Civic é uma pechincha!"
  ]

  pokemon = [
    "Pokémon! Temos que pegar!"
  ]

  laugh = [
    "kkkkkkkkkkkkkk",
    "hahahahahahaha",
    "huahaushaushau",
    "huehuehuehuehue",
    "lol"
  ]

  lol = [
    "lol",
    "lot of laughs ou League of Legends?"
  ]

  malebolgia = [
    "Malebolgia? O Eduardo disse que pegaria.",
    "Lembra quando ela saiu da porta dos desesperados?",
    "Ela tinha um telefone com linha direta no inferno"
  ]

  referency = [
    "Referência? \nEu consigo entender algumas!",
    "Percebe Ivair, a referência do cavalo?",
    "Eu entendi a referência!"
  ]

  steam = [
    "Steam!? Salvem suas carteiras!",
    "Maldito Gabe...",
    "Oh não! Outra promo no steam... \nHaja dinheiro...",
    "Steam? Garanto que você tem 300 jogos que nunca sequer foram instalados."
  ]

  headphone = [
    "Eu quero meu headphone!!!",
    "Vai querer me enganar que essa porra na cabeça aí é um penteado?"
  ]

  black_hat = [
    "Eu acho que esse penico preto tá afetando seu cerebro!"
  ]

  rabelo = [
    "Cuidado com o Luiz Rabelo! Ele tem o poder do diário...",
    "Graaaande Rabelão! \nTinha namorada no Ceará, Rio Grande do Sul e até no Acre!",
    "Graças as aulas do Rabelão, hoje vocês sabem tudo de TVs e carros.\nInclusive que o Honda Civic é o melhor carro do mundo!",
    "Acho que o Rabelão pegava a Malebolgia..."
  ]

  duck = [
    "qua qua qua qua",
    "Pato é a puta que te pariu!"
  ]

  robot.hear /(.*)spoiler(.*)/i, (res) ->
    res.send res.random spoiler

  robot.hear /(.*)civic(.*)/i, (res) ->
    res.send res.random civic

  robot.hear /(.*)revisão(.*)civic(.*)/i, (res) ->
    res.send "A primeira revisão do Honda Civic custa apenas R$337,53"

  robot.hear /(.*)pokemon(.*)/i, (res) ->
    res.send res.random pokemon

  robot.hear /\b(k{3,}|[ha]{3,})\b/i, (msg) ->
    number = Math.floor(Math.random() * (20 - 1 + 1) + 1)
    if number % 3 == 0
      msg.send msg.random laugh

  robot.hear /(.*)malebolgia(.*)/i, (res) ->
    res.send res.random malebolgia

  robot.hear /(.*)(vc (é|e|eh) um rob(o|ô) ou (é|e|eh) um pato|voc(ê|e) (é|e|eh) um rob(o|ô) ou (é|e|eh) um pato)(.*)/i, (res) ->
    res.send res.random duck

  robot.hear /(.*)pato(.*)/i, (res) ->
    res.send res.random duck

  robot.hear /(.*)jarvis(.*)pato(.*)/i, (res) ->
    res.send res.random duck

  robot.hear /(.*)lol(.*)/i, (res) ->
    res.send res.random lol

  robot.hear /(.*)League of Legends(.*)/i, (res) ->
    res.send "Jogo de viado..."

  robot.hear /(.*)headphone|head fone(.*)/i, (res) ->
    res.send res.random headphone

  robot.hear /(.*)p(e|i)nico preto(.*)/i, (res) ->
    res.send res.random black_hat

  robot.hear /(.*)refer(ê|e)ncia|refer(ê|e)ncias(.*)/i, (res) ->
    res.send res.random referency

  robot.hear /(.*)rabel(o|ao|ão)(.*)/i, (res) ->
    res.send res.random rabelo